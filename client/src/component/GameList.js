import React from 'react';
import * as firebase from "firebase/app";
import GameItem from "./GameItem"
import Info from "./Info"
class GameList extends React.Component {

  constructor(props) {
    super(props)
    let user = firebase.auth().currentUser;

    console.log('GameList constructor user', user);
    this.state = {user:user};
    //this.fetchGames = fetchGames.bind(this)


  }
  componentDidMount () {
    //this.fetchGame();
    console.log('GameList componentDidMount');

    this.fetchGames();


  }

  fetchGames(){
    const config_get = {
      mode: 'cors',
      method:'get',
      headers: {'Access-Control-Allow-Origin':'*','Content-Type':'application/json'},
    }
    fetch('http://127.0.0.1:5000/games', config_get)
            .then((response) => response.json())
            .then((json) => {

              console.log('fetchGames:', json)

              var stateObj = {
                games: json
              }

              this.setState(stateObj)

              console.log('state:', this.state)
            })
            .catch((error) => {
              console.error(error);
            });
  }

  newGame(){
    const config_post = {
      mode: 'cors',
      method:'post',
      headers: {'Access-Control-Allow-Origin':'*','Content-Type':'application/json'},
    }
    return fetch('http://127.0.0.1:5000/newgame', config_post)
            .then((response) => response.json())
            .then((json) => {return})
            .catch((error) => {
              console.error(error);
            });
  }

  getItems = () => {
    let items =[]
    if(this.state.games){
      this.state.games.map((item, i) =>{
        let status = (item.playerX != null && item.playerO != null)?'playing': 'waiting'
        items.push(<GameItem roomId={item.id} playerO={item.playerO} playerX={item.playerX} status={status}/>)

      });
    }
    return items;
  }

  render() {
    return (
      [<Info user={this.state.user.email}/>,
        <div className="container">
        <div className="row">
          <div className="col-6 text-center p-2" style={{float: 'none',
margin: '0 auto'}}>
            Game List
          </div>
        </div>
        </div>,

      <div className="container">

        {this.getItems()}


      </div>,

      <div className="container">
        <div className="row">
          <div className="col-6">
            <div className="btn btn-primary" onClick={()=>{this.newGame().then(()=>this.fetchGames())}}>New Game</div>

          </div>

        </div>

      </div>
      ]
    );



  }
}

export default GameList;
