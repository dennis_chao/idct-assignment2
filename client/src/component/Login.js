import React from 'react';
import $ from 'jquery';
import * as firebase from "firebase/app";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useHistory,
  useLocation
} from "react-router-dom";


class Login extends React.Component {
  constructor(props) {
    super(props);
    console.log('Login props:',this.props);
    this.history = props.history;
  }
  componentWillMount() {
          firebase.auth().onAuthStateChanged((user) => {
              if (user) {
                  //this.setState({loading:false,user})
                  this.props.history.push('/games');
              } else {
                  //this.setState({loading:false})
              }
          });
      }

  login (props){

      var email = $('#login').val();
       let pwd = $('#password').val()
       console.log('login:'+email+', password:'+pwd);
console.log('login props:',props);

       firebase.auth().signInWithEmailAndPassword(email, pwd).then(userCredential =>{
           let user = userCredential.user;
           console.log('auth user:'+user.email);
           $('#errorMsg').html("");
           $('div.login').hide();
           // const board = new Board ({
           //     el : document.getElementById('root')
           // });
           //this.props.history.push('/game');


       }).catch(function(error) {
           // Handle Errors here.
           var errorCode = error.code;
           var errorMessage = error.message;
           console.log('errorCode:'+errorCode+', errorMsg:'+errorMessage);
           $('#errorMsg').html("<span>Login Fail</span>");
       });
  }

  render(){
    return [
    <span class="title">Tic Tac Toe</span>,
    <form id="loginForm">
      <input type="text" id="login" class="fadeIn second" name="email" placeholder="login" defaultValue="test1@idct.com" />
      <input type="password" id="password" class="fadeIn third" name="login" placeholder="password" defaultValue="abc123" />
      <div id="errorMsg"><span></span></div>
      <input type="button" class="submit fadeIn fourth" value="Log In" onClick={this.login} />
    </form>
  ]
  }
}

export default Login;
