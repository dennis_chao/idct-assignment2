import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useHistory,
  useLocation
} from "react-router-dom";

class GameItem extends React.Component {

  state = {
    redirect: false
  }

  handleClick = (event)=>{
    let gameId = event.currentTarget.dataset.gameid;
    this.setState({
          redirect: true
        })
  }

  renderRedirect = (gameId) => {
    if (this.state.redirect) {
      return   <Redirect to={{
          pathname: '/game/'+gameId
        }}/>
    }
  }

  render() {
    return (
      <div>
      {this.renderRedirect(this.props.roomId)}
      <div className="row gameItem mb-2 " data-gameid={this.props.roomId} onClick={this.handleClick}>
        <div className="col-12 pt-2 pb-2">
          <div className="row">
            <div className="col">
              {this.props.roomId}
            </div>
          </div>
          <div className="row">
            <div className="col">
              PlayerX:
            </div>
            <div className="col">
              {this.props.playerX}

            </div>
          </div>
          <div className="row">
            <div className="col">
              PlayerO:
            </div>

            <div className="col">
              {this.props.playerO}

            </div>
          </div>
          <div className="row">
            <div className="col">
              Status:
            </div>
            <div className="col">
              {this.props.status}

            </div>
          </div>
        </div>

      </div>
      </div>

    )
  }
}

export default GameItem
