import React from 'react';
import Board from './Board';
import Info from './Info';
import { roomSocket, gameMoveSocket, gameJoinSocket } from '../socket';
import * as firebase from "firebase/app";
import io from 'socket.io-client';


const calculateWinner = (squares) => {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  for (let i = 0; i < lines.length; i += 1) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return { winner: squares[a], winnerRow: lines[i] };
    }
  }

  return { winner: null, winnerRow: null };
};

const getLocation = (move) => {
  const locationMap = {
    0: 'row: 1, col: 1',
    1: 'row: 1, col: 2',
    2: 'row: 1, col: 3',
    3: 'row: 2, col: 1',
    4: 'row: 2, col: 2',
    5: 'row: 2, col: 3',
    6: 'row: 3, col: 1',
    7: 'row: 3, col: 2',
    8: 'row: 3, col: 3',
  };

  return locationMap[move];
};

const initialState = {
  id:'',
  history: [
    {
      squares: Array(9).fill(null),
    },
  ],
  currentStepNumber: 0,
  xIsNext: true,
  xPlayer: null,
  oPlayer: null
};
const config_get = {
  mode: 'cors',
  method:'get',
  headers: {'Access-Control-Allow-Origin':'*','Content-Type':'application/json'},
}
const config_post = {
  mode: 'cors',
  method:'post',
  headers: {'Access-Control-Allow-Origin':'*','Content-Type':'application/json'},
}

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id:props.gameId,
      history: [
        {
          squares: Array(9).fill(null),
        },
      ],
      currentStepNumber: 0,
      xIsNext: true,
      xPlayer: null,
      oPlayer: null
    };

    console.log('Game constructor state:', this.state);
    //this.joinGame = this.joinGame.bind(this);
  }



  componentDidMount () {
    //this.fetchGame();
    console.log('Game componentDidMount');
    let user = firebase.auth().currentUser;
    console.log('Game componentDidMount user, '+user);
    this.setState({user:user});






    roomSocket('uid',this.state.id, (json, err)=>{
      console.log('room Socket err:', err);
      var stateObj = {
        id: json.id,
        xIsNext: json.xIsNext,
        history: json.moves,
        xPlayer: json.playerX,
        oPlayer: json.playerO,
        currentStepNumber: json.currentStepNo
      }
      this.setState(stateObj);
    });
  }

  fetchGame(){
    fetch('http://127.0.0.1:5000/game/5Mg7yqvzsKEUHfigopAU', config_get)
            .then((response) => response.json())
            .then((json) => {
              var stateObj = {
                id: json.id,
                xIsNext: json.xIsNext,
                history: json.moves,
                xPlayer: json.playerX,
                oPlayer: json.playerO,
                currentStepNumber: json.currentStepNo
              }


              this.setState(stateObj)
            })
            .catch((error) => {
              console.error(error);
            });
  }



  joinGame(joinPlayerX){
    console.log('joinGame, X?', joinPlayerX)
    console.log('state?', this.state)
    // const data ={
    //   uid:this.state.user.id,
    //   joinPlayerX:joinPlayerX}
    // const config = {
    //   mode: 'cors',
    //   method:'post',
    //   body: JSON.stringify(data),
    //   headers: {'Access-Control-Allow-Origin':'*','Content-Type':'application/json'},
    // }
    // let url = 'http://127.0.0.1:5000/game/'+this.state.id+'/join'
    //
    // fetch(url, config)
    //   .then((response) => response.json())
    //   .then((json) => {
    //     console.log('joinGame json:', json);
    //   })
    //   .catch((error) => {
    //     console.error(error);
    //   });
    gameJoinSocket(this.state.user.uid, this.state.id, joinPlayerX);
  }

  // handleClick(i) {
  //   let url = 'http://127.0.0.1:5000/game/'+this.state.id+'/move'
  //   const data ={
  //     target:i,
  //     isX:this.state.xIsNext?'true':'false'
  //   }
  //   const config = {
  //     mode: 'cors',
  //     method:'post',
  //     body: JSON.stringify(data),
  //     headers: {'Access-Control-Allow-Origin':'*','Content-Type':'application/json'},
  //   }
  //   fetch(url, config)
  //     .then((res) => res.json())
  //     .then((json) => {
  //       console.log('handleClick:', json);
  //     })
  //     .catch((error) => {
  //       console.log('handleClick error:', error);
  //     })
  // }
  handleClick(i){
    let isX = this.state.xIsNext?'true':'false';
    let user = firebase.auth().currentUser;
    gameMoveSocket(user.uid, this.state.id, i, isX);

  }

  jumpTo(step) {
    this.setState({
      currentStepNumber: step,
      xIsNext: step % 2 === 0,
    });
  }

  sortMoves() {
    this.setState({
      history: this.state.history.reverse(),
    });
  }

  reset() {
    this.setState(initialState);
  }

  render() {
    const { history, xPlayer, oPlayer } = this.state;
    console.log('history:',history)
    console.log('currentStep', this.state.currentStepNumber);
    const current = history[this.state.currentStepNumber];
    console.log('current:'+current)
    const { winner, winnerRow } = calculateWinner(current.squares);

    const moves = history.map((step, move) => {
      const currentLocation = step.currentLocation ? `(${step.currentLocation})` : '';
      const desc = step.stepNumber ? `Go to move #${step.stepNumber}` : 'Go to game start';
      const classButton = move === this.state.currentStepNumber ? 'button--green' : '';

      return (
        <li key={move}>
          <button className={`${classButton} button`} onClick={() => this.jumpTo(move)}>
            {`${desc} ${currentLocation}`}
          </button>
        </li>
      );
    });

    const PlayerInfo = (props) =>{
      console.log('PlayerInfo props:',props);

      if(props.player != null){

        return (<span>{props.player}</span>)
      }
      else{
        return <span className="btn btn-primary" onClick={props.click}> Join </span>
      }
    }

    let status;
    if (winner) {
      status = `Winner ${winner}`;
    } else if (history.length === 10) {
      status = 'Draw. No one won.';
    } else {
      status = `Next player: ${this.state.xIsNext ? 'X' : 'O'}`;
    }

    return (
      [
      <div className="container game">
        <div className="row">
          <div className="col game-info">
            <div>X Player: <PlayerInfo player={xPlayer} click={() => this.joinGame('true')}/></div>
            <div>O Player:  <PlayerInfo player={oPlayer} click={() => this.joinGame('false')}/></div>

            {/*<ol>{moves}</ol>*/}
          </div>
        </div>
        <div className="row jsutify-content-center">
          <div className="col-6">
            {status}

          </div>

        </div>
        <div className="row mt-3">
          <div className="col game-board ">
            <Board
              squares={current.squares}
              winnerSquares={winnerRow}
              onClick={i => this.handleClick(i)}
            />
          </div>
        </div>
      </div>
      ]
    );
  }
}

export default Game;
