import React from 'react';


class Info extends React.Component {

  constructor(props) {
    super(props);

    console.log('INfo props', props)
    this.state = {
      user: props.user
    };
  console.log('INfo state', this.state)
    //console.log('Game constructor state:', this.state);
    //this.joinGame = this.joinGame.bind(this);
  }

  render(){
    return (
      <div className="container">
      <div className="row rowInfo p-1">

        <div className="col-8 align-self-center">
          <div style={{'font-szie':'medium'}}>Hello {this.state.user}</div>

        </div>
        <div className="col-4 text-center">
          <div className="btn btn-primary">Log out</div>

        </div>
      </div>
      </div>
    )
  }
}

export default Info;
