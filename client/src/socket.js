import openSocket from 'socket.io-client';

var options = {
          rememberUpgrade:true,
          transports: ['websocket'],
          secure:true,
          rejectUnauthorized: false
              }
const  socket = openSocket('http://localhost:5000');
function roomSocket(uid, gameId, cb) {
  // socket.on('timer', timestamp => cb(null, timestamp));
  // socket.emit('subscribeToTimer', 1000);
  console.log('roomSocket');
  console.log('socket.connected', socket.connected);


  socket.on('connect', () => socket.emit('enterRoom', uid, gameId));

  // 监听消息
      socket.on('msg', function (userName, msg) {
        console.log('on msg:', userName+" "+msg);
      });

      // 监听系统消息
      socket.on('sys', function (sysMsg, users) {
        console.log('on sys:', sysMsg+" "+users);
      });

      socket.on('game', function(game, game2){
        console.log('on game', game)
        cb(game, null);
      })
  if(socket.connected){
    socket.emit('enterRoom', uid, gameId)
  }

}

function gameMoveSocket(uid, gameId, targetIdx, isX, cb){
  let data = {
    gameId: gameId,
    uid: uid,
    targetIdx: targetIdx,
    isX: isX
  }
  socket.emit('gameMove', data);
}

function gameJoinSocket(uid, gameId, isX, cb){
  let data = {
    gameId: gameId,
    uid: uid,
    isX: isX
  }
  console.log('gameJoinSocket data:', data)
  socket.emit('gameJoin', data);

}
export {gameMoveSocket};
export { roomSocket };
export { gameJoinSocket };
