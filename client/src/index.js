

import React from "react"
import ReactDOM from 'react-dom';

import './css/bootstrap.css';
import './index.css';
import './css/login.css';
import './css/style.css';
import Game from './component/Game';
import GameList from './component/GameList'
import Login from './component/Login';


import {$,jQuery} from 'jquery';
// export for others scripts to use

import * as firebase from "firebase/app";

// Add the Firebase services that you want to use
import "firebase/auth";
import {
  BrowserRouter,
  Route,
  Redirect,
  Link,
  useHistory,
  useLocation
} from 'react-router-dom';


const firebaseConfig = {
  apiKey: "AIzaSyCw44t6FK74wX0EnLtj6Zx-E6m8mIbGikg",
  authDomain: "idct-assigment.firebaseapp.com",
  databaseURL: "https://idct-assigment.firebaseio.com",
  projectId: "idct-assigment",
  storageBucket: "",
  messagingSenderId: "730991085902",
  appId: "1:730991085902:web:d69870b375fe260784761f",
  measurementId: "G-S0C48G5TDZ"
};
firebase.initializeApp(firebaseConfig);



function PrivateRoute({ children, ...rest }) {
  let user = firebase.auth().currentUser;

  let history = useHistory();

  return (
    <Route
      {...rest}
      render={({ location }) =>
        user ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location }
            }}
          />
        )
      }
    />
  );
}

ReactDOM.render((
   <BrowserRouter>

        <PrivateRoute path="/games" >
          <GameList/>
        </PrivateRoute>
        <Route path="/game/:gameId" render={(props)=>{
          return <Game gameId={props.match.params.gameId}/>
        }} >

        </Route>
        <Route exact path="/login" component={Login}/>
<Route exact path="/" component={Login}/>

   </BrowserRouter >
), document.getElementById( 'root' ) )
