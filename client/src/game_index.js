import React from 'react';
import ReactDOM from 'react-dom';
import Game from './component/Game';

import './css/bootstrap.min.css';
import './index.css';
ReactDOM.render(<Game />, document.getElementById('root'));
