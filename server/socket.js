var server = require('./index.js');
var io = require('socket.io')(server, {origins:'*:*'});
//io.set('origins', 'http://localhost:3000');



var roomInfo = {};
io.on('connection', function(socket){
  // 获取请求建立socket连接的url
    // 如: http://localhost:3000/room/room_1, roomID为room_1
    // var url = socket.request.headers.referer;
    // var splited = url.split('/');
    // var roomID = splited[splited.length - 1];   // 获取房间ID
    var user = '';


    var roomID


    socket.on('enterRoom', function (userName, gameId) {

      user = userName;
      console.log(user + ' joining room ' + gameId);
      roomID = gameId;
      // 将用户昵称加入房间名单中
      if (!roomInfo[gameId]) {
        roomInfo[gameId] = [];
      }
      roomInfo[gameId].push(user);

      socket.join(roomID);    // 加入房间
      // 通知房间内人员
      io.to(roomID).emit('sys', user + ' joined ', roomInfo[gameId]);

      var game =  controller_game.getGame(gameId).then(game=>{
        console.log('emmitting game', game);
        io.to(roomID).emit('game', game);
      });

      console.log(user + ' joined room ' + gameId);
    });

    socket.on('leave', function () {
      socket.emit('disconnect');
    });

    socket.on('disconnect', function () {
      // 从房间名单中移除
      var index = roomInfo[roomID].indexOf(user);
      if (index !== -1) {
        roomInfo[roomID].splice(index, 1);
      }

      socket.leave(roomID);    // 退出房间
      io.to(roomID).emit('sys', user + ' out ', roomInfo[roomID]);
      console.log(user + ' out ' + roomID);
    });

    // 接收用户消息,发送相应的房间
    socket.on('message', function (msg) {
      // 验证如果用户不在房间内则不给发送
      if (roomInfo[roomID].indexOf(user) === -1) {
        return false;
      }
      io.to(roomID).emit('msg', user, msg);
    });
});

module.exports = io;
