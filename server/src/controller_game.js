
const express = require('express')
var cookieParser = require('cookie-parser')
const app = express()
const cors = require("cors");
const bodyParser = require('body-parser');
const userService = require('./user-firebase');
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(cookieParser())
var admin = require("firebase-admin");

let db = admin.firestore();
app.use(cors({ origin: true }));
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/game/:gameId',  (req, res) =>{
  if(req.cookies){

    const sessionCookie = req.cookies.session || '';
    let gameId = req.params.gameId
    console.log('getting game:', gameId);
    let gamesRef = db.collection('games');

    gamesRef.doc(gameId).get().then((doc) => {
      let game = doc.data();
      console.log('/game game:' + JSON.stringify(game));
      game.id = doc.id;
      res.jsonp(game);
      return;
    }).catch((err) =>{
      console.log('Error getting doc', err);
      res.send(err);
      return;
    });
  }
  return;
});

app.post('/newgame', (req, res) =>{
  console.log('newgame')
  let gamesRef = db.collection('games');
  addNewGame(gamesRef,req, res);
  return;
})

app.get('/games', (req, res) =>{
    if(req.cookies){
        console.log('getting game');
      const sessionCookie = req.cookies.session || '';
        let gamesRef = db.collection('games');
        //res.send('Hello Game!');
        gamesRef
            .orderBy('createTimeStamp','desc')
            .where('winner.winner', '==', null)
            .get()
            .then(snapshot =>{
                // console.log(snapshot);
                // console.log('size:'+snapshot.size);
                if(snapshot.empty){
                    console.log('No matching documents.');
                    addNewGame(gamesRef,req, res);
                    return;
                }else{
                    let games = [];
                    snapshot.forEach(doc =>{
                        let game = doc.data();
                        console.log('game:' + JSON.stringify(game));
                        game.id = doc.id;
                        games.push(game);
                    })
                    res.jsonp(games);
                    return;
                }
            })
            .catch(err =>{
                console.log('Error getting doc', err);
                return;
            })

    }else{
        console.log('cookies is unavailable');
        res.status(401).send('cookies is unavailable');
    }
    return;
  }
);

app.post('/game/:gameId/move', (req, res) =>{
  console.log("game/move: "+JSON.stringify(req.body));
  const gameId = req.params.gameId;
  const targetIdx = req.body.target;
  const isX = req.body.isX;

  let gamesRef = db.collection('games');

  let gameRef = gamesRef.doc(gameId);
  let transaction = db.runTransaction(t => {
    return t.get(gameRef)
      .then(doc => {
        const game = doc.data();
        let lastMove = game.moves.slice(-1).pop();

        // check if invalid input
        if(calculateWinner(lastMove.squares).winner != null){
          return Promise.reject('End Game Already');
        }else if( lastMove.squares[targetIdx] != null){
          return Promise.reject('Invalid Move');
        }else if(game.playerX ==null|| game.playerO == null){
          return Promise.reject('member not ready')
        }


        // prepare new data to update
        let newSquares = lastMove.squares.slice();
        newSquares[targetIdx] = isX=='true'?'X':'O'
        console.log('game moves length', game.moves.length);
        game.moves = game.moves.concat([
          {
            squares:newSquares,
            stepNumber: game.moves.length,
          }
        ]);
        console.log('game moves length after concat ', game.moves.length);
        lastMove = game.moves.slice(-1).pop();

        let currentStepNumber = game.moves.length-1;
        let winner = calculateWinner(lastMove.squares);

        //update

        t.update(gameRef, {
          moves: game.moves,
          xIsNext: !isX,
          winner: winner,
          currentStepNo: currentStepNumber
        });
        return Promise.resolve('Game updated');

      });
  }).then(result => {
    console.log('Transaction success', result);
    res.send('update succes');
  }).catch(err => {
    console.log('Transaction failure:', err);
    res.sendStatus('update failure: '+err);
  });
  //res.sendStatus(200);
  return;
});

app.post('/game/:gameId/join', (req, res) =>{
  const gameId = req.params.gameId;
  const uid = req.body.uid
  const isJoinPlayerX = req.body.joinPlayerX;

  let gamesRef = db.collection('games');
  let gameRef = gamesRef.doc(gameId);
 let transaction = gameJoin(gameRef, uid, isJoinPlayerX).then( result=>{
    console.log('final result:',result);
    gamesRef.doc(gameId).get().then(doc=>{

      res.jsonp(doc.data());
      return
    })
    return
  }).catch( err =>{
    console.log('final error:',err);
    res.send(err)
    return
  });
  return;
});


const calculateWinner = (squares) => {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  for (let i = 0; i < lines.length; i += 1) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return { winner: squares[a], winnerRow: lines[i] };
    }
  }

  return { winner: null, winnerRow: null };
};

function getGame(gameId){
  console.log('getting game:', gameId);
  let gamesRef = db.collection('games');

  return gamesRef.doc(gameId).get()
  .then((doc) => {
    let game = doc.data();
    console.log('game:' + JSON.stringify(game));
    game.id = doc.id;
    return game;
  }).catch((err) =>{
    console.log('Error getting doc', err);
    return null;
  });

}


function gameMove(gameRef, targetIdx, isX, user_email){
  return transaction = db.runTransaction(t => {
    return t.get(gameRef)
      .then(doc => {
        const game = doc.data();
        let lastMove = game.moves.slice(-1).pop();

        // check if invalid input
        if(calculateWinner(lastMove.squares).winner != null){
          return Promise.reject('End Game Already');
        }else if( lastMove.squares[targetIdx] != null){
          return Promise.reject('Invalid Move');
        }else if(game.playerX ==null|| game.playerO == null){
          return Promise.reject('member not ready')
        }else if(game.xIsNext == true && game.playerX != user_email){
            return Promise.reject('You are not player X');
        }else if(game.xIsNext == false && game.playerO != user_email){
            return Promise.reject('You are not player O');
        }


        // prepare new data to update
        let newSquares = lastMove.squares.slice();
        newSquares[targetIdx] = isX=='true'?'X':'O'
        console.log('game moves length', game.moves.length);
        game.moves = game.moves.concat([
          {
            squares:newSquares,
            stepNumber: game.moves.length,
          }
        ]);
        console.log('game moves length after concat ', game.moves.length);
        lastMove = game.moves.slice(-1).pop();

        let currentStepNumber = game.moves.length-1;
        let winner = calculateWinner(lastMove.squares);

        //update

        t.update(gameRef, {
          moves: game.moves,
          xIsNext: isX =='true'?false: true,
          winner: winner,
          currentStepNo: currentStepNumber
        });
        return Promise.resolve('Game updated');

      });
  })
}

function gameJoin(gameRef, uid, isJoinPlayerX){
  return  transaction = db.runTransaction(t => {
      return t.get(gameRef)
        .then(doc => {
          const game = doc.data();
          console.log('gameJoin retrieved game:', game);
          // check if invalid input
          if((isJoinPlayerX =='true' && game.playerX != null) || (isJoinPlayerX == 'false' && game.playerO != null)){
            return Promise.reject('Not able to join');
          }
          return userService.getUser(uid).then(
            (userRecord)=>{
              console.log('Join Game - Successfully fetched user data:', userRecord.toJSON());
              return userRecord;
            })
            .catch((err)=>{
              console.log(err);
              return  Promise.reject('Get User Fail');
            });
        })
        .then(userRecord =>{
          let updateObj;
          if(isJoinPlayerX == 'true'){
            updateObj ={
              playerX: userRecord.email
            }
          }else{
            updateObj ={
              playerO: userRecord.email
            }
          }
          t.update(gameRef, updateObj)
        })
        .then(()=>{
          // return gamesRef.doc(gameId).get().then(doc=>{
          //   return doc.data()
          // })

          return Promise.resolve('Game updated');
        })
        .catch(err =>{
          return Promise.reject(err);
        });

    })
}

function addNewGame(gamesRef, req, res){
    let date = new Date();
    let newGame = {
        playerX: null,
        playerO: null,
        winner: {winner:null},
        moves: [
          {
            squares: Array(9).fill(null),
          },

        ],
        xIsNext:true,
        createTimeStamp: date.getTime(),
        createTimeString: date.toLocaleString(),
        currentStepNo: 0
    };


    gamesRef.add(newGame).then((docRef) => {
        //console.log(docRef);
        return docRef.get();
        })
        .then((queryDocSnapshot) =>{
            console.log('addedNewGame ',queryDocSnapshot.data());
            let game = queryDocSnapshot.data();
            game.id = queryDocSnapshot.id;
            res.jsonp(game);
            return;

        })
        .catch(err =>{
            console.log('Error create doc', err);
            return;
        });
}





module.exports = {

	app :app,
  getGame: getGame,
  gameMove:gameMove,
  gameJoin:gameJoin

}
