
var admin = require("firebase-admin");
const express = require('express')
var cookieParser = require('cookie-parser')
const app = express()
app.use(cookieParser())
const defaultPort = 3000

const userService = require('./user-firebase');

// app.listen(process.env.PORT || defaultPort, () => {
// 	console.log('Server started on port '+(process.env.PORT || defaultPort));
// });


// app.get('/', (req, res) => res.send('Hello World!'));

app.route('/user/reg')
  .get((req, res) => {
    console.log('GET: /user/reg')
    var email = req.query.email
    var password = req.query.password
    var displayName = req.query.displayName
    addUser(res, email, password, displayName);
  })
  .post((req, res) => {
    console.log('POST: /user/reg')
    var email = req.body.email
    var password = req.body.password
    var displayName = req.body.displayName
    addUser(res, email, password, displayName);
  })

app.get('/users', (req, res) => {

  userService.listUser(5,(users, pageToken) => {
    console.log('pageToken:'+pageToken)

    res.status(200).send({users:users,pageToken:pageToken});
  })

})

app.get('/users/:uid', (req, res) => {
  let uid = req.params.uid;

  userService.getUser(uid).then(
		(userRecord)=>{
			console.log('Successfully fetched user data:', userRecord.toJSON());
			res.jsonp(userRecord);
			return
		}).catch((err)=>{
			console.log(err);
			res.send('fail')
			return
		});

})

app.route('/user/login')
  .get( (req, res) => {
    var email = req.query.email
    var password = req.query.password
    login(res, email, password)
  })
  .post((req, res) => {


    var email = req.body.email
    var password = req.body.password
    console.log('email:'+email);
    login(res, email, password)
  })

app.route('/user/logout')
  .all((req, res) => {
    const sessionCookie = req.cookies.session || '';
   res.clearCookie('session');
   admin.auth().verifySessionCookie(sessionCookie)
   .then((decodedClaims) => {
     return admin.auth().revokeRefreshTokens(decodedClaims.sub);
   })
   .then(() => {
     res.redirect('/login');
     return;
   })
   .catch((error) => {
      res.redirect('/login');
    });
  }
)

function addUser(res, email, password, displayName){
  userService.addUser(email, password,displayName, (error, userRecord) => {
    if(error){
      console.log('error:'+error);
      res.status(500).send(error.errorMessage);
    }else{
      res.status(200).send(userRecord.toJSON());
    }

  })
}

function login(res, email, password){
  userService.signIn(email, password, (error, userCredential) => {
    if(error){
      console.log('error:'+error.message);
      return res.status(401).send(error.code);
    }else{

      userCredential.user.getIdToken().then((idToken) => {  // <------ Check this line
         console.log('idToken:'+idToken); // It shows the Firebase token now
           //console.log('signIn getIdToken:'+userCredential.user.getIdToken());
           // Set session expiration to 5 days.
         const expiresIn = 60 * 60 * 24 * 5 * 1000;
         // Create the session cookie. This will also verify the ID token in the process.
         // The session cookie will have the same claims as the ID token.
         // To only allow session cookie setting on recent sign-in, auth_time in ID token
         // can be checked to ensure user was recently signed in before creating a session cookie.
         admin.auth().createSessionCookie(idToken, {expiresIn})
           .then((sessionCookie) => {
              // Set cookie policy for session cookie.
               console.log('create session Cookie:'+sessionCookie);
              const options = {maxAge: expiresIn, httpOnly: true, secure: true};
              res.cookie('session', sessionCookie, options);
              //res.end(JSON.stringify({status: 'success'}));
              // res.redirect('../game/');
              res.send(userCredential);
              return;
           }, error => {
            res.status(401).send('UNAUTHORIZED REQUEST!');
            return;
           }).catch( error =>{
               console.log('error:'+error);
               return;
         });
         return;
      }).catch((error)=>{
          console.log('error:'+error);
        });
      //res.status(200).send(userCredential);
      return;
    }
  })
}

module.exports = {

	app :app

}
