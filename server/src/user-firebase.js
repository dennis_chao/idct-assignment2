var firebase = require("firebase");
require("firebase/auth");
var admin = require("firebase-admin");
function addUser(email, password, displayName, callback) {

  admin.auth().createUser({
    email: email,
    emailVerified: false,
    password: password,
    displayName: displayName,
    disabled: false
  })
  .then(function(userRecord) {
    // See the UserRecord reference doc for the contents of userRecord.
    console.log('Successfully created new user:', userRecord.uid);
    callback(null, userRecord);
    return userRecord;
  })
  .catch(function(error) {
    console.log('Error creating new user:', error);
    callback(error, null);
  });
}

function listUsers(size, callback, nextPageToken) {
  console.log('listUser size:'+size);
  // List batch of users, 1000 at a time.

  admin.auth().listUsers(size, nextPageToken)
    .then(function(listUsersResult) {
        users = [];

      listUsersResult.users.forEach(function(userRecord) {
        users.push(userRecord);
      });
      // if (listUsersResult.pageToken) {
      //   // List next batch of users.
      //   listAllUsers(size, callback, listUsersResult.pageToken);
      // }else{
        callback(users, listUsersResult.pageToken);
      //}
      return users;
    })
    .catch(function(error) {
      console.log('Error listing users:', error);
    });
}

function getUser(uid){
  return admin.auth().getUser(uid)
  .then(function(userRecord) {
    // See the UserRecord reference doc for the contents of userRecord.

    return userRecord;
  })
  .catch(function(error) {
    console.log('Error fetching user data:', error);
    return Promise.reject('Not able to join');
  });
}


function signIn(email, password, callback){
  console.log('signIn:'+email+'/'+password);
  firebase.auth().signInWithEmailAndPassword(email, password)
  .then(function(userCredential){
    console.log('userCredential email:'+userCredential.user.email);



    callback(null, userCredential);
    //idToken = userCredential.user.stsTokenManager.accessToken;

    return userCredential;


  })

  .catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    console.log('errorCode:'+errorCode+', errorMessage:'+errorMessage);
    callback(error, null);
  });
}

function signOut(callback){
  console.log('signOut');
  firebase.auth().signOut().then(function() {
      console.log('signOut ok');
      callback('ok');
      return 'ok';
    }).catch(function(error) {
      console.log('errorCode:'+errorCode+', errorMessage:'+errorMessage);
      callback(error);
    });
}

module.exports = {

	addUser : addUser,
  listUser: listUsers,
  signIn: signIn,
  signOut: signOut,
  getUser: getUser,

}
