var firebase = require("firebase/app");

var admin = require("firebase-admin");

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
const userService = require('./src/user-firebase');
var port = process.env.PORT || 5000;


var serviceAccount = require("./service_account/service_account.json");

admin.initializeApp({
  //credential: admin.credential.applicationDefault(),
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://idct-assigment.firebaseio.com"
});
const firebaseConfig = {
  apiKey: "AIzaSyCw44t6FK74wX0EnLtj6Zx-E6m8mIbGikg",
  authDomain: "idct-assigment.firebaseapp.com",
  databaseURL: "https://idct-assigment.firebaseio.com",
  projectId: "idct-assigment",
  storageBucket: "",
  messagingSenderId: "730991085902",
  appId: "1:730991085902:web:d69870b375fe260784761f",
  measurementId: "G-S0C48G5TDZ"
};
firebase.initializeApp(firebaseConfig);




const controller_user = require('./src/controller_user');
const controller_game = require('./src/controller_game');

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

let db = admin.firestore();
var roomInfo = {};
io.on('connection', function(socket){
  // 获取请求建立socket连接的url
    // 如: http://localhost:3000/room/room_1, roomID为room_1
    // var url = socket.request.headers.referer;
    // var splited = url.split('/');
    // var roomID = splited[splited.length - 1];   // 获取房间ID
    var user = '';
    var roomID;
    socket.on('enterRoom', function (userName, gameId) {

      user = userName;
      console.log(user + ' joining room ' + gameId);
      roomID = gameId;
      // 将用户昵称加入房间名单中
      if (!roomInfo[gameId]) {
        roomInfo[gameId] = [];
      }
      roomInfo[gameId].push(user);

      socket.join(roomID);    // 加入房间
      // 通知房间内人员
      io.to(roomID).emit('sys', user + ' joined ', roomInfo[gameId]);

      var game =  controller_game.getGame(gameId).then(game=>{
        console.log('emmitting game', game);
        io.to(roomID).emit('game', game);
      });

      console.log(user + ' joined room ' + gameId);
    });

    socket.on('leave', function () {
      console.log('socket leave');
      socket.emit('disconnect');
    });

    socket.on('disconnect', function () {



      var url = socket.request.headers.referer;
      console.log('disconnect url:', url);
      // 从房间名单中移除
      // var index = roomInfo[roomID].indexOf(user);
      // if (index !== -1) {
      //   roomInfo[roomID].splice(index, 1);
      // }
      //
      // socket.leave(roomID);    // 退出房间
      // io.to(roomID).emit('sys', user + ' out ', roomInfo[roomID]);
      console.log(user + ' out ' + roomID);
    });

    // 接收用户消息,发送相应的房间
    socket.on('message', function (msg) {
      // 验证如果用户不在房间内则不给发送
      if (roomInfo[roomID].indexOf(user) === -1) {
        return false;
      }
      io.to(roomID).emit('msg', user, msg);
    });

    socket.on('gameMove', function(data){
      console.log('gameMove data:', data)
      let gameId = data.gameId
      let uid = data.uid;
      let targetIdx = data.targetIdx;
      let isX = data.isX;

      let gamesRef = db.collection('games');
      let gameRef = gamesRef.doc(gameId);

      userService.getUser(uid).then(
        (userRecord)=>{
          return userRecord.email
        }
      ).then(user_email =>{
        controller_game.gameMove(gameRef, targetIdx, isX, user_email)
        .then(result => {
          console.log('Transaction success', result);
          var game =  controller_game.getGame(gameId).then(game=>{
            console.log('emmitting game', game);
            io.to(roomID).emit('game', game);
          });
        }).catch(err => {
          console.log('Transaction failure:', err);
          //res.sendStatus('update failure: '+err);
        });
      })
      .catch(err =>{
        console.log('getUser failure:', err);
      })




    });

    socket.on('gameJoin', function(data){
      console.log('gameJoin data:', data);
      let gameId = data.gameId
      let uid = data.uid;

      let isX = data.isX;
      let gamesRef = db.collection('games');
      let gameRef = gamesRef.doc(gameId);
      controller_game.gameJoin(gameRef, uid, isX).then(result => {
        console.log('Transaction success', result);
        var game =  controller_game.getGame(gameId).then(game=>{
          console.log('emmitting game', game);
          io.to(roomID).emit('game', game);
        });
      }).catch(err => {
        console.log('Transaction failure:', err);
        //res.sendStatus('update failure: '+err);
      });
    });
});

// http.listen(port, function(){
//   console.log('listening on *:' + port);
// });
app.use("/",controller_user.app)
app.use("/",controller_game.app)
http.listen(process.env.PORT || port, () => {
	console.log('Server started on port '+(process.env.PORT || port));
});
